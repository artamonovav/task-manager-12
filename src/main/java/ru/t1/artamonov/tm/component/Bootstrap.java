package ru.t1.artamonov.tm.component;

import ru.t1.artamonov.tm.api.*;
import ru.t1.artamonov.tm.controller.CommandController;
import ru.t1.artamonov.tm.controller.ProjectController;
import ru.t1.artamonov.tm.controller.TaskController;
import ru.t1.artamonov.tm.repository.CommandRepository;
import ru.t1.artamonov.tm.repository.ProjectRepository;
import ru.t1.artamonov.tm.repository.TaskRepository;
import ru.t1.artamonov.tm.service.CommandService;
import ru.t1.artamonov.tm.service.ProjectService;
import ru.t1.artamonov.tm.service.TaskService;
import ru.t1.artamonov.tm.util.TerminalUtil;

import static ru.t1.artamonov.tm.constant.ArgumentConst.*;
import static ru.t1.artamonov.tm.constant.TerminalConst.*;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void processCommand(final String command) {
        if (command == null) {
            commandController.showErrorCommand();
            return;
        }
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_EXIT:
                System.exit(0);
                break;
            case CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case CMD_COMMANDS:
                commandController.showCommands();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CMD_PROJECT_CREATE:
                projectController.createProject();
                break;
            case CMD_PROJECT_LIST:
                projectController.showProjects();
                break;
            case CMD_TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CMD_TASK_CREATE:
                taskController.createTask();
                break;
            case CMD_TASK_LIST:
                taskController.showTasks();
                break;
            case CMD_PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CMD_PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CMD_PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CMD_PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CMD_PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CMD_PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CMD_TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CMD_TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CMD_TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CMD_TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CMD_TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CMD_TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            default:
                commandController.showErrorCommand();
                break;
        }
    }

    private void processCommands() {
        commandController.showWelcome();
        while (true) {
            System.out.print("\nENTER COMMAND: ");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    private void processArgument(final String argument) {
        if (argument == null) {
            commandController.showErrorArgument();
            return;
        }
        switch (argument) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ARGUMENTS:
                commandController.showArguments();
                break;
            case ARG_COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showErrorArgument();
                break;
        }
    }

    private boolean processArguments(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        processArgument(arg);
        return true;
    }

    public void run(final String[] args) {
        if (processArguments(args)) System.exit(0);
        processCommands();
    }

}
