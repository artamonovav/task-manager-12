package ru.t1.artamonov.tm.api;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showHelp();

    void showWelcome();

    void showErrorArgument();

    void showErrorCommand();

    void showInfo();

    void showCommands();

    void showArguments();

}
