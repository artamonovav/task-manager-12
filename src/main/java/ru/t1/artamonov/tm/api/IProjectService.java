package ru.t1.artamonov.tm.api;

import ru.t1.artamonov.tm.enumerated.Status;
import ru.t1.artamonov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByIndex(Integer index, Status status);

    void clear();

    Project create(String name);

    Project create(String name, String description);

    List<Project> findAll();

    void remove(Project project);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
